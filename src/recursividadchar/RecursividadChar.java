/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursividadchar;

import javax.swing.JOptionPane;

/**
 *
 * @author SALGADO
 */
public class RecursividadChar {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        JOptionPane.showMessageDialog(null,"La palabra creada es: \n"+crearPalabraConChar() );
        
    }
    
    public static String crearPalabraConChar(){
       String palabraCompleta="";
       char espacioAscii = 32;
       
       char caracterIngresado = JOptionPane.showInputDialog("Ingrese una letra ó barra espaciadora para finalizar").charAt(0);
       
        if (caracterIngresado != espacioAscii) {
            return palabraCompleta += caracterIngresado + crearPalabraConChar();
        }
        return palabraCompleta;
    }
    
}
